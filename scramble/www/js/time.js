function addSeconds(){

    clearInterval(gameTimer);

    var timeOld = localStorage.getItem('time');
    var newTime = parseInt(timeOld) + 5;

    localStorage.setItem('time', newTime);

}

function removeSeconds(){
    clearInterval(gameTimer);

    var timeOld = localStorage.getItem('time');
    var newTime = parseInt(timeOld) - 3;

    localStorage.setItem('time', newTime);
}

function timer(timeTotal){
    timeTotal = parseInt(timeTotal);
    gameTimer = setInterval(function(){

        timeTotal--;

        localStorage.setItem('time', timeTotal);

        document.getElementById('timerCountDown').textContent = timeTotal;
        if(timeTotal <= 0){
            alert("you have lost the game, trying again now...");


            clearInterval(gameTimer);
            localStorage.clear();

            document.getElementById("streak").innerHTML = "0";
            document.getElementById('timerCountDown').textContent = "30";

            timer(30);

            prepareQuestions();
        }

    }, 750);
}
