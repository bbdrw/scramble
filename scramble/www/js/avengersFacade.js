/**
 * File Name: avengersFacade.js
 *
 * Revision History:
 *       Sabbir Ahmed, 2018-02-22 : Created
 */


function showCalculatedAge() {
    var dob = $("#txtDOBAdd").val();
    $("#txtAgeAdd").val(getCurrentAge(dob));
}

function addFriendEnemy() {
    // 1. test validation
    if (doValidate_frmAddFriendEnemy()) {
        // 2. if validation is successful then fetch the info from input controls
        var name = $("#txtNameAdd").val();
        var fullName = $("#txtFullNameAdd").val();
        var isFriend = $("#radFriendAdd").prop("checked");
        var dob = $("#txtDOBAdd").val();

        // 3. insert into table (by calling insert DAL function and supplying inputs)
        var options = [name, fullName, dob, isFriend ];
        function callback() {
            console.info("Success: Record inserted successfully");
        }
        Friend.insert(options, callback);
    }
    else{
        console.error("Validation failed");
    }
}

function testValidation() {
    if (doValidate_frmExtra()) {
        console.info("Validation successful");
    }
    else{
        console.error("Validation failed");
    }
}

function clearDatabase() {
    var result = confirm("really want to clear database?");
    if (result) {
        try {
            DB.dropTables();
            alert("Database cleared!");
        } catch (e) {
            alert(e);
        }


    }
}

function updateFriendEnemy() {
    // var id = $("#txtId").val();
    var id = localStorage.getItem("id");

    var name = $("#txtNameModify").val();
    var fullName = $("#txtFullNameModify").val();
    var dob = $("#txtDOBModify").val();
    var isFriend = $("#radFriendModify").prop("checked");

    //very important
    var options = [name, fullName, dob, isFriend, id];
    function callback() {
        console.info("Success: Record updated successfully");
    }
    Friend.update(options, callback);
}

function deleteFriendEnemy() {
    // var id = $("#txtId").val();
    var id = localStorage.getItem("id");

    var options = [id];
    function callback() {
        console.info("Success: record deleted successfully");
        $.mobile.changePage("#pageFriends", {transition: 'none'});

    }
    Friend.delete(options, callback);
}

function showAllFriendsEnemies() {
    var options = [];
    function callback(tx, results) {

        var htmlcode = "";


        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            console.info("Id: " + row['id'] +
            " Name: " + row['name'] +
                " Full Name: " + row['fullName'] +
                " DOB: " + row['dob'] +
                " Is Friend: " + row['isFriend']);

            htmlcode += "<li><a data-role='button' data-row-id=" + row['id'] + " href='#'>" +
                "<h1>Name: " + row['name'] + "</h1>" +
                "<h2>Full Name: " + row['fullName'] + "</h2>" +
                "<h3>DOB: " + row['dob'] + "</h3>" +
                "<h3>Is Friend: " + row['isFriend'] + "</h3>" +
                "</a></li>";
        }

        var lv = $("#lvAll");
        lv = lv.html(htmlcode);
        lv.listview("refresh"); //important

        $("#lvAll a").on("click", clickHandler);

        function clickHandler() {
            localStorage.setItem("id", $(this).attr("data-row-id") );
            // both will work
            $.mobile.changePage("#pageDetail", {transition: 'none'});
            // $(location).prop('href', '#pageDetail');
        }


    }
    Friend.selectAll(options, callback);
}

function showOneFriendEnemy() {
    // var id = $("#txtId").val();
    var id = localStorage.getItem("id");

    var options = [id];
    function callback(tx, results) {
        var row = results.rows[0];

        console.info("Id: " + row['id'] +
            " Name: " + row['name'] +
            " Full Name: " + row['fullName'] +
            " DOB: " + row['dob'] +
            " Is Friend: " + row['isFriend']);

        $("#txtNameModify").val(row['name']);
        $("#txtFullNameModify").val(row['fullName']);
        $("#txtDOBModify").val(row['dob']);

        if (row['isFriend'] == 'true') {
            $("#radFriendModify").prop("checked", true);
        }
        else{
            $("#radEnemyModify").prop("checked", true);
        }

        $("#frmModifyFriendEnemy :radio").checkboxradio("refresh");


    }
    Friend.select(options, callback);
}