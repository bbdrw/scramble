
var countryDict = {};
var imgLocation = {};
var soundEffectCorrect;
var gameTimer;

var t = 0;

function checkPair(key){

    if(key in countryDict == false){
        countryDict[key] = 1;
    }

}

function checkImageLocation(key,img){

    if(key in imgLocation == false){
        imgLocation[key] = img;
    }

}

function init() {
    localStorage.clear();
    loadFlag();


    var ranNum = getRandomNum(2);
    setBlock(ranNum);

}

function setBlock(ranNum){
    if(ranNum == 0){
        document.getElementById("BottomRect").style.visibility = "visible";
        document.getElementById("TopRect").style.visibility = "hidden";

        document.getElementById("question").style.marginBottom = "7%";
        document.getElementById("flag_question_image").style.removeProperty('margin-top');

    }
    else{
        document.getElementById("BottomRect").style.visibility = "hidden";
        document.getElementById("TopRect").style.visibility = "visible";

        document.getElementById("question").style.removeProperty('margin-bottom');
        document.getElementById("flag_question_image").style.marginTop = "7%";
    }
}

function createArray(stringArray){

    var realArray = stringArray.split(",");

    return realArray;
}

function chooseFiller(question, rand1, rand2, rand3){
    var array = [question, rand1, rand2, rand3];

    if(localStorage.getItem("randomList") == null || localStorage.getItem("randomList") == "" ){
        var randomNumber = getRandomNum(3);

        var localStorageValue = randomNumber + ",";
        localStorage.setItem("randomList", localStorageValue);

        if(randomNumber == 0){

           var tempArray = createArray(question);

            localStorage.setItem("answerText", tempArray[0]);
        }

        return array[randomNumber];
    }
    else{

        var tempString = localStorage.getItem("randomList");
        var randoArray = tempString.split(",");
        var randomNum = 0;

        do{
            randomNum = getRandomNum(4);
            var isTrue = true;

            for(var i = 0; i < randoArray.length - 1;i++){
                if(parseInt(randomNum) == parseInt(randoArray[i])){
                    isTrue = false;
                    break;
                }

            }

        }while(isTrue == false);

        var localStorageValue = tempString + randomNum + ",";
        localStorage.setItem("randomList", localStorageValue);

        if(randomNum == 0){
            var tempArray = createArray(question);

            localStorage.setItem("answerText", tempArray[0]);
        }

        return array[randomNum];

    }


}

function repopulate(){
    $('#flag_question_image').empty();

    var html = "<tr style='width: 100%;'>";
    var realArray;

    for(var x = 0; x < 4; x++){
        switch(x){
            case 0:
                var stringArray = localStorage.getItem("block01");
                realArray = stringArray.split(",");
                break;
            case 1:
                var stringArray = localStorage.getItem("block02");
                realArray = stringArray.split(",");
                break;
            case 2:
                var stringArray = localStorage.getItem("block03");
                realArray = stringArray.split(",");
                html += "</tr> <tr>";
                break;
            case 3:
                var stringArray = localStorage.getItem("block04");
                realArray = stringArray.split(",");
                break;

        }

        html +=  " <td class='draggable'><a data-role='button' data-theme='a' href='#' data-row-id=" + realArray[0] + ">\n" +
            "       <img style='border-radius: 17%;' src=" + realArray[1] + "\n width='75px' height='75px' alt='canada'>\n" +
            "   </a>\n</td> ";

    }


    html +=  " </tr>";
    document.getElementById('flag_question_image').innerHTML = html;

    document.getElementById("streak").innerHTML = localStorage.getItem("streak");

    x = 0;

    var $dragging = $('.draggable').draggabilly();


    $("#flag_question_image a").on("pointerup", clickHandler);


    function clickHandler() {
        try {

            var itemTop = $(this).offset().top;
            var itemLeft = $(this).offset().left;
            var itemHeight = $(this).height();
            var itemWidth = $(this).width();


            var boxTop;
            var boxLeft;
            var boxWidth;
            var boxHeight;

            if(document.getElementById("BottomRect").style.visibility == "hidden"){
                boxTop = $('#TopRect').offset().top;
                boxLeft = $('#TopRect').offset().left;
                boxHeight = $('#TopRect').height();
                boxWidth = $('#TopRect').width();
            }
            else{
                boxTop = $('#BottomRect').offset().top;
                boxLeft = $('#BottomRect').offset().left;
                boxHeight = $('#BottomRect').height();
                boxWidth = $('#BottomRect').width();
            }

            if (((itemLeft + itemWidth) > boxLeft && itemLeft < (boxLeft + boxWidth)) && ((itemTop + itemHeight) > boxTop && itemTop < (boxTop + boxHeight))) {
                checkAnswer($(this).attr("data-row-id"));
            }
            else{
               // alert("Incorrect");
               localStorage.setItem('isWronge', "false");
                localStorage.setItem('time', localStorage.getItem('time'));
               navigator.vibrate(500);
                repopulate();
            }
        }catch(e){
            console.log(e.toString());
        }
    }

}

function populateTable(){
    document.getElementById("streak").innerHTML = "0";

    var html = "<tr style='width: 100%;'>";

    var question = localStorage.getItem("answer");
    var rand1 = localStorage.getItem("rand1");
    var rand2 = localStorage.getItem("rand2");
    var rand3 = localStorage.getItem("rand3");

    var realArray;


    var x = 0;


    for(var i = 0; i < 4; i++){

        switch(i){
            case 0:
                var type = chooseFiller(question, rand1, rand2, rand3);
                //realArray = createArray(question);
                realArray = createArray(type);
                if(localStorage.getItem("answerText") != null && localStorage.getItem("answerText") != ""){
                    document.getElementById('question').innerHTML = realArray[0];
                    localStorage.removeItem("answerText")
                }
                break;
            case 1:
                var type = chooseFiller(question, rand1, rand2, rand3);
                //realArray = createArray(rand1);
                realArray = createArray(type);
                if(localStorage.getItem("answerText") != null && localStorage.getItem("answerText") != ""){
                    document.getElementById('question').innerHTML = realArray[0];
                    localStorage.removeItem("answerText")
                }
                break;
            case 2:
                var type = chooseFiller(question, rand1, rand2, rand3);
                //realArray = createArray(rand2);
                realArray = createArray(type);
                if(localStorage.getItem("answerText") != null && localStorage.getItem("answerText") != ""){
                    document.getElementById('question').innerHTML = realArray[0];
                    localStorage.removeItem("answerText")
                }
                html += "</tr> <tr>";
                break;
            case 3:
                var type = chooseFiller(question, rand1, rand2, rand3);
                //realArray = createArray(rand3);
                realArray = createArray(type);
                if(localStorage.getItem("answerText") != null && localStorage.getItem("answerText") != ""){
                    document.getElementById('question').innerHTML = realArray[0];
                    localStorage.removeItem("answerText")
                }
                break;

        }

        html +=  " <td class='draggable'><a data-role='button' data-theme='a' href='#' data-row-id=" + realArray[0] + ">\n" +
            "       <img style='border-radius: 17%;' src=" + realArray[1] + "\n width='75px' height='75px' alt='canada'>\n" +
            "   </a>\n</td> ";
        x++;

        var stringHolder = realArray[0] + "," + realArray[1];
        switch(i){
            case 0:
                localStorage.setItem("block01", stringHolder);
                break;
            case 1:
                localStorage.setItem("block02", stringHolder);
                break;
            case 2:
                localStorage.setItem("block03", stringHolder);
                break;
            case 3:
                localStorage.setItem("block04", stringHolder);
                break;
        }
    }

    html +=  " </tr>";
    document.getElementById('flag_question_image').innerHTML = html;

    document.getElementById("streak").innerHTML = "0";

    x = 0;

    var $dragging = $('.draggable').draggabilly();

    $("#flag_question_image a").on("pointerup", clickHandler);

    function clickHandler() {
        try {


            var itemTop = $(this).offset().top;
            var itemLeft = $(this).offset().left;
            var itemHeight = $(this).height();
            var itemWidth = $(this).width();


            var boxTop;
            var boxLeft;
            var boxWidth;
            var boxHeight;

            if(document.getElementById("BottomRect").style.visibility == "hidden"){
                boxTop = $('#TopRect').offset().top;
                boxLeft = $('#TopRect').offset().left;
                boxHeight = $('#TopRect').height();
                boxWidth = $('#TopRect').width();
            }
            else{
                boxTop = $('#BottomRect').offset().top;
                boxLeft = $('#BottomRect').offset().left;
                boxHeight = $('#BottomRect').height();
                boxWidth = $('#BottomRect').width();
            }


            if (((itemLeft + itemWidth) > boxLeft && itemLeft < (boxLeft + boxWidth)) && ((itemTop + itemHeight) > boxTop && itemTop < (boxTop + boxHeight))) {
                checkAnswer($(this).attr("data-row-id"));
            }
            else{
                navigator.vibrate(500);
                localStorage.setItem('time', localStorage.getItem('time'));
                repopulate();
            }
        }catch(e){
            console.log(e.toString());
        }
    }

}


function checkAnswer(userInput){

    var stringAnswer = localStorage.getItem("answer");

    var answerArray = stringAnswer.split(",");

    if(userInput == answerArray[0]){

        var ranNum = getRandomNum(2);
        setBlock(ranNum);

        var timeUpdate = parseInt(localStorage.getItem('time')) + 5;

        document.getElementById('timerCountDown').innerHTML = timeUpdate.toString() ;

        navigator.vibrate([150,150,400,150,150]);

        $('#winAudio').attr("src", "Sounds/correct.wav");

        addSeconds();

        var audio = document.getElementById('winAudio');
        audio.play();
        //alert("correct");
        if(localStorage.getItem("streak") == null || localStorage.getItem("streak") == ""){
            var int = 1;
            localStorage.setItem("streak", int.toString());

            localStorage.setItem("answersCorrect", "1");
        }
        else{
            var oldStreak = parseInt(localStorage.getItem("streak"));
            var newStreak = oldStreak + 1;
            localStorage.setItem("streak", newStreak.toString());

            var answersCorrectOld = parseInt(localStorage.getItem("answersCorrect"));
            var answerCorrectNew = answersCorrectOld + 1;

            localStorage.setItem("answersCorrect", answerCorrectNew.toString());
        }

        localStorage.removeItem("randomList");
        localStorage.removeItem("answerText");
        var totalCorrect = parseInt(localStorage.getItem("answersCorrect"));

        if(totalCorrect == 18){
            alert("you have won the game");
            alert("starting new game");

            clearInterval(gameTimer);
            localStorage.clear();

            document.getElementById("streak").innerHTML = "0";

            timer(30);
            t = 0;
            prepareQuestions();
        }
        else{
            timer(parseInt(localStorage.getItem('time')));
            createNewQuestion();
        }
    }
    else{
        localStorage.setItem("streak", "0");
        navigator.vibrate(500);
        $('#winAudio').attr("src", "Sounds/incorrect.wav");

        var audio = document.getElementById('winAudio');
        audio.play();

        var timeUpdate = parseInt(localStorage.getItem('time')) - 3;

        document.getElementById('timerCountDown').innerHTML = timeUpdate.toString() ;

        removeSeconds();
        timer(parseInt(localStorage.getItem('time')));

        repopulate();

    }

    document.getElementById("streak").innerHTML = localStorage.getItem("streak");
}

function createNewQuestion(){
    localStorage.removeItem("usedQuestions");

    prepareQuestions();
}

function getRandomNum(countryNum){
   return Math.floor((Math.random() * countryNum) + 0)
}

function setUsedQuestions(addedNum){
    var stringArray = localStorage.getItem("usedQuestions");

    if(stringArray == null || stringArray == ""){
        localStorage.setItem("usedQuestions", addedNum);
    }
    else{
        stringArray += "," + addedNum;
        localStorage.setItem("usedQuestions", stringArray);
    }

}

function randomizeNum(cond,countryNum){

    var randomNum = getRandomNum(countryNum);
    var numArray = [];

    if(cond != "first"){

        var tempString = localStorage.getItem("usedQuestions");

        numArray = tempString.split(",");

        do{
            randomNum = getRandomNum(countryNum);
            var isTrue = true;

            for(var i = 0; i < numArray.length;i++){
                if(parseInt(randomNum) == parseInt(numArray[i])){
                    isTrue = false;
                    break;
                }

            }

        }while(isTrue == false);

        setUsedQuestions(randomNum);

    }
    else{
        if(localStorage.getItem("questionDone") != null && localStorage.getItem("questionDone") != ""){

            var tempArray = localStorage.getItem("questionDone");
            var checkArray = tempArray.split(",");

            do{
                randomNum = getRandomNum(countryNum);
                var isTrue = true;

                for(var i = 0; i < checkArray.length;i++){

                    if(parseInt(randomNum) == parseInt(checkArray[i])){
                        isTrue = false;
                        break;
                    }

                }

            }while(isTrue == false);

            var questionsDone = localStorage.getItem("questionDone");
            questionsDone += "," + randomNum;

            localStorage.setItem("questionDone", questionsDone);
        }
        else{
            localStorage.setItem("questionDone", randomNum);
        }

        setUsedQuestions(randomNum);

    }

    var x = 0;
    for(var keys in imgLocation){

        if(x == randomNum){

            switch(localStorage.getItem("turn")){
                case "1":
                    localStorage.setItem('answer', keys + "," + imgLocation[keys]);
                    break;
                case "2":
                    localStorage.setItem('rand1', keys + "," + imgLocation[keys]);
                    break;
                case "3":
                    localStorage.setItem('rand2', keys + "," + imgLocation[keys]);
                    break;
                case "4":
                    localStorage.setItem('rand3', keys + "," + imgLocation[keys]);
                    break;

            }
            break;
        }
        x++;
    }


}

function prepareQuestions(){

    var countryNum = Object.keys(countryDict).length;
    document.getElementById("streak").innerHTML = "0";

    for(var i = 0; i < 4; i++) {
        var loc = i + 1;
        localStorage.setItem("turn", loc.toString());
        if (i > 0) {
            randomizeNum("second", countryNum);
        }
        else {
            randomizeNum("first", countryNum);
        }
    }

    populateTable();

}

function loadFlag(){
    var options = [];

    function callback(tx, results) {
        var row = results;

        var x = 0;

        for(var i = 0; i < results.rows.length; i++){
            x++;
        }

        var flagArray = [];

        for (var i = 0; i < results.rows.length; i++) {
            row = results.rows[i];

            flagArray.push([row['countryName'],row['countryFlagImage']]);
            checkPair(row['countryName'], 1);
            checkImageLocation(row['countryName'],row['countryFlagImage']);

        }

        localStorage.setItem('time', '30');
        timer(parseInt(localStorage.getItem('time')));

        prepareQuestions();
        //******USED TO CYCLE THROUGH DICTIONARY******
        /*var mainString = "";

        for(var keys in countryDict){
            mainString += countryDict[keys];
        }

        alert(mainString);*/

    }
    myFlags.selectAllFlags(options, callback);

}


function initDB() {
    console.info("Creating Database ...");

    try {
        DB.createDatabase();
        if (db) {
            clearDatabaseItem();

            DB.createCountries();
            DB.fillCountries();
            DB.createFlags();
            DB.fillFlag();
        }
        else{
            console.error("Error: Cannot create tables : Database not available");
        }

    } catch (e) {
        console.error("Error: (Fatal) Error in initDB(). Cannot proceed.");
    }
}


$(document).ready(function () {
    initDB();

    document.getElementById('timerCountDown').innerHTML = "30";

    init();
});
