/**
 * File Name: util.js
 *
 * Revision History:
 *       Sabbir Ahmed, 2018-02-22 : Created
 */


/**
 * Calculates the age
 * @param dob Date of Birth
 * @returns {number} The calculated age
 */
function getCurrentAge(dob) {
    var year = Number(dob.substr(0,4));
    var month = Number(dob.substr(5,2)) - 1;
    var day = Number(dob.substr(8,2));

    var today = new Date();
    var age = today.getFullYear() - year;
    if (today.getMonth()< month || (today.getMonth() == month && today.getDate() < day)) {
        age--;
    }

    return age;
}


function doValidate_login(){
    var form = $("#loginForm");
    form.validate({
        rules:{
            loginEmail:{
                foundEmail: true
            },
            loginPassword:{
                foundPassword: true
            }
        },
        messages:{
            loginEmail:{
                foundEmail: "The email entered was not found"
            },
            loginPassword:{
                foundPassword: "The password provided does not match our records please try again"
            }

        }
    });
    return form.valid();

}

jQuery.validator.addMethod("foundPassword",
    function(value, element){

        if(localStorage.getItem("isFound") == "false"){
            return this.optional(element) || false;
        }

       var password = value;
       var user = localStorage.getItem("loginuserid");

       var options = [user,password];
        function callback(tx, results) {
            //var row = results.rows[0];

        }

        myLogin.checkPassword(options, callback);

        if(localStorage.getItem("myBool") == "true"){
            return this.optional(element) || true;
        }
         else{
            return this.optional(element) || false;
        }

    },
    "Must be conestoga email");

jQuery.validator.addMethod("foundEmail",
    function(value, element){

        var email = value;

        var options = [email];
        function callback(tx, results) {
            var row = results.rows[0];


            localStorage.setItem("loginuseremail", row['email']);
            //localStorage.setItem("loginuserid", email);

            localStorage.setItem("loginuserid", row['userid']);

        }
        myLogin.select(options, callback);

        if((localStorage.getItem("loginuserid") == null || localStorage.getItem("loginuserid") == "") || localStorage.getItem("loginuseremail") != email ){
            localStorage.removeItem("loginuseremail");
            localStorage.setItem("isFound", "false");
            return this.optional(element) || false;
        }
        else{
            localStorage.removeItem("isFound");
            return this.optional(element) || true;

        }
    },
    "No record found from email");


function doValidate_frmAddFriendEnemy() {
    var form = $("#frmAddFriendEnemy");
    form.validate({
        rules:{
            txtNameAdd:{
                required: true,
                minlength: 2
            },
            txtFullNameAdd:{
                required: true,
                rangelength: [2, 20]
            },
            txtDOBAdd:{
                required: true,
                // min: "1990-01-01"
                agecheck: true
            }
        },
        messages:{
            txtNameAdd:{
                required: "You must enter name",
                minlength: "Name must be at least 2 characters long"
            },
            txtFullNameAdd:{
                required: "You must enter full name",
                rangelength: "Full name must be 2-20 characters long"
            },
            txtDOBAdd:{
                required: "DOB is a must",
                // min: "Must be born after 1990-01-01"
                agecheck: "you must be at least 2 years old"
            }
        }
    });
    return form.valid();

}


jQuery.validator.addMethod("agecheck",
    function(value, element){
        var age = getCurrentAge(value);
        if (age >= 2) {
            return true;
        }
        return false;
    },
    "You must be at least 2 years old");


function doValidate_frmExtra() {
    var form = $("#frmExtra");
    form.validate({
        rules:{
            txtPassword:{
                required: true,
                minlength: 8,
                passwordcheck: true
            },
            txtVerifyPassword:{
                required: true,
                equalTo: "#txtPassword"
            },
            txtUrl:{
                required: true
            },
            txtEmail:{
                required: true,
                emailcheck: true
            }
        },
        messages:{
            txtPassword:{
                required: "You must enter Password ",
                minlength: "Password must be at least 8 characters long",
                passwordcheck: "Must contain 1 cap and 1 number"
            },
            txtVerifyPassword:{
                required: "Please re-enter password",
                equalTo: "Password does not match, re-enter"
            },
            txtUrl:{
                required: "Please enter a valid url"
            },
            txtEmail:{
                required: "You must enter email",
                emailcheck: "Email must be a conestoga email"
            }
        }
    });
    return form.valid();
}


jQuery.validator.addMethod("passwordcheck",
    function(value, element){
        var regex = /([A-Za-z\d]*[A-Z]+[A-Za-z\d]*[\d]+[A-Za-z\d]*)|([A-Za-z\d]*[\d]+[A-Za-z\d]*[A-Z]+[A-Za-z\d]*)/;
        return this.optional(element) || regex.test(value);
    },
    "Must contain 1 cap and 1 number");

jQuery.validator.addMethod("emailcheck",
    function(value, element){
        var regex = /^.+conestogac.on.ca$/;
        return this.optional(element) || regex.test(value);
    },
    "Must be conestoga email");

